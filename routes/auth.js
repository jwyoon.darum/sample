/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------

//--- Login화면 표시
router.get("/bnk/login", (req, res) => {
	util.log("Login 화면");
	res.cookie(__ACCESS_TOKEN_NAME, "");
	res.cookie('ck-auth', "");
	res.render("login/login", { mode: "get", resCode :"", errorCode:"",  errorMessage :"", errorCustomerMessage:"" });
});
//--------------


//--- 인증처리 로직
router.post("/bnk/login", (req, res) => {
	util.log("Login Process");
	let body = req.body;
	

	var url = __AUTH_API_URI+"/api/auth/login";
	console.log('######### URL: ', url);

	axios.post(url, 
		{
			username: body.username,
			password: body.password			
		}
	)
	.then((ret) => {
		let resCode = "";
		let ck = "";
		let data = "";

		if(ret.status == 200) {
			util.log(ret.headers);
			util.log("##### Gerated Access Token=>"+ret.data.data);
			success = ret.data.success;
			data = ret.data.data;
			ck = ret.data.ck;

			result_info = util.uridecode(ret.data.result_info).split('|');

			resCode = result_info[0];
			errorCode = result_info[1];
			errorMessage = result_info[2];
			errorCustomerMessage = result_info[3];
		}

		// 로그인 성공
		if(success===true)
		{
			util.log("##### Gerated Access Token=>"+ret.data.data);
			res.cookie(__ACCESS_TOKEN_NAME, ret.data.data);		//cookie에 임시로 저장
			res.cookie('ck-auth', ret.data.ck);		//cookie에 임시로 저장
			res.redirect("/bnk/list");
		}
		// 로그인 실패
		else 
		{
			util.log("Login 화면");
			res.cookie(__ACCESS_TOKEN_NAME, "");
			res.render("login/login", { mode: "post", resCode :resCode, errorCode:errorCode,  errorMessage :errorMessage, errorCustomerMessage:errorCustomerMessage });
		}

		
	})
	.catch((error) => {
		console.error(error);
		res.redirect("/bnk/login");
	});	
});
//-----------


//---- Logout
router.get("/bnk/logout", (req, res) => {
	util.log("Logout 화면");
	
	res.redirect("/bnk/login");
});
//-----------

module.exports = router;