/*jshint esversion: 6 */

//---- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
var querystring = require('querystring');
const util = require(__BASEDIR + '/util');
//---------------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//----------

//--- 거래내역 조회 화면 표시
router.get("/bnk/account_detail", (req, res) => {
	util.log("거래내역조회 화면");
	let body = req.body;

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("__ACCESS_TOKEN_NAME => " + __ACCESS_TOKEN_NAME);
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('_headers: ', _headers);
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	render_history(res, body, _headers);
});

router.post("/bnk/account_detail", (req, res) => {
	util.log("거래내역조회 화면");
	let body = req.body;

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('_headers: ', _headers);
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	render_history(res, body, _headers);
});

//랜더링
var render_history = function(res, body, _headers)
{
	axios.defaults.withCredentials = true;

	var acno = body.account_no;
	var PDT_CD = body.product_cd;
	var set_month = body.set_month;

	if(typeof acno == "undefined" || acno == null || acno == "")
	{
		acno = "";
	}
	if(typeof PDT_CD == "undefined" || PDT_CD == null || PDT_CD == "")
	{
		PDT_CD = "";
	}
	if(typeof set_month == "undefined" || set_month == null || set_month == "")
	{
		set_month = 6;
		// DMN : 입출금통장
		// ISV : 적금
		// SID : 예금
		// TRS : 신탁
		if(body.product_gb && body.product_gb=="ISV") set_month = 1;
	}

	var month_term = strdt = edt = "";
	
	if(set_month>0)
	{
		strdt = util.get_date(set_month);
		edt = util.get_date(0);

		month_term = set_month+"개월";
	}
	else
	{
		strdt = body.strdt;
		edt = body.edt;

		month_term = "직접입력";
	}
	//console.log('######### set_month: ', set_month);
	//console.log('######### strdt: ', strdt);
	//console.log('######### edt: ', edt);

	//console.log('__ACCESS_TOKEN_NAME: ', __ACCESS_TOKEN_NAME);
	//console.log('product_gb: ', body.product_gb);
	//console.log('set_month: ', set_month);

	var param = {
		'ib20_cur_mnu': 'MWPINMB000INM10',
		'acno': acno,
		'PDT_CD': PDT_CD,
		'strdt': strdt,
		'edt': edt,
		'jwt_token': _headers[__ACCESS_TOKEN_NAME], 
		'cookie': _headers['cookie']
	};
	console.log('######### param: ', param);

	var url = __API_PRODUCT_URI+'/bnk/history';
	console.log('######### URL: ', url);

	axios.post(url, 
	param,
	{
		headers: _headers
	})
	.then((ret) => {
		if(ret.status == 200) {
			
			var ret_body_ = ret.data._msg_._body_;
			/*
			ACNO_FMT // 계좌번호
			ACNO //계좌번호
			ACNT_MNGM_NM //계좌명
			
			ACNT_BAMT_AMT_FMT // 현잔액
			START_DT
			END_DT

			ACNO_LIST = { //전계좌리스트
				PDT_NM // 계좌명
				ACNO_FMT // 계좌번호
				ACNO //
			}
			body.set_month
			IDN12KBN08800103V00_REC1
			*/
			var data = {};
			data.account_list = ret_body_['ACNO_LIST'];	 //전계좌 리스트
			data.trade_list = ret_body_['IDN12KBN08800103V00_REC1'];	 //거래내역 리스트

			data.account_no = ret_body_['ACNO_FMT'];	 //현 계좌번호
			data.ACNO = ret_body_['ACNO'];	 //현 계좌번호2
			data.account_nm = ret_body_['ACNT_MNGM_NM'];	 //현 계좌명
			data.account_money = ret_body_['ACNT_BAMT_AMT_FMT'];	 //현 계좌잔액

			data.start_dt = ret_body_['START_DT'];	 //시작일
			data.end_dt = ret_body_['END_DT'];	 //계좌명

			data.set_month = set_month;	 //ACNO_LIST
			data.month_term = month_term;	 //ACNO_LIST
			console.log('######### data: ', data);
			
			res.render("views/history"
				,{
					data : data
				}
			);
			console.log('############## show history');
			//util.log("##### Gerated Access Token=>"+ret.data.data);
		} else {
		}
	})
	.catch((error) => {
		//console.error(error);
		res.redirect("/bnk/login");
	});	
}



module.exports = router;
