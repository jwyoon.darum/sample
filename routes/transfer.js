/*jshint esversion: 6 */

//---- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
var querystring = require('querystring');
const util = require(__BASEDIR + '/util');
//---------------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//----------

//--- 전계좌 조회 화면 표시
router.get("/bnk/transfer", (req, res) => {
	util.log("이체 화면1");

	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	_headers['cookie'] = req.cookies['ck-auth'].join('; ');
	_headers['Content-Type'] = 'application/json;charset=UTF-8';
	
	console.log('Cookies: ', req.cookies);
	console.log('token: ', token);
	
	axios.defaults.withCredentials = true;

	var param = {
		'SECR_MDM_VALID': 'Y',
		'ib20_cur_mnu': 'MWPTWD1000TWD10',
		'TRN_FLAG': 'N',
		'jwt_token': _headers[__ACCESS_TOKEN_NAME],
		'cookie': _headers['cookie']
	};

	var url = __API_TRANSFER_URI+'/bnk/transfer1';
	console.log('######### URL: ', url);

	axios.post(url, 
		param,
		{
			headers: _headers
		}
	)
	.then((ret) => {
		if(ret.status == 200) {

			var ret_body_ = ret.data._msg_._body_;
			console.log("###### ret_body_", ret_body_);
			/*
			
        DFRY_ACNT_YN // 출금가능계좌
        BNK_PDT_NNM_CNTN // 계좌명
        PSTLY_BAMT_AMT_FMT // 현재잔액
        ACNO_FMT : 출금계좌번호
		DFRY_PSBL_BAMT_FMT : 출금가능금액
		
		DFRY_ACNT_LIST
			*/
			var data = {};
			data.transfer_ok_account = ret_body_['DFRY_ACNT_LIST'];

			data.transfer_ok_account.forEach(function(item,idx)
			{
				if(item.DFRY_ACNT_YN == "1") {
					data.account_nm = item.BNK_PDT_NNM_CNTN;
					data.account_no = item.ACNO_FMT;
					data.account_ok_money = item.DFRY_PSBL_BAMT_FMT;
					data.account_money = item.PSTLY_BAMT_AMT_FMT;
				}
				else 
				{
					return true;
				}
			});

			console.log('######### data: ', data);
			
			res.render("views/transfer"
				,{
					data : data
				}
			);
			console.log('############## show list');
			//util.log("##### Gerated Access Token=>"+ret.data.data);
		} else {
		}
	})
	.catch((error) => {
		//console.error(error);
		res.redirect("/bnk/login");
	});	
});




/*
//메인화면
router.get("/bnk/main", (req, res) => {
	util.log("메인화면");
	//main 으로 들어오면 바로 페이징 처리
	res.redirect('/bnk/list' );
	//res.redirect('/bnk/paging/' + 1);
});


//-- List
router.get("/bnk/paging/:cur", (req, res) => {

	//--- userinfo
	let cu = util.userData;
	util.log("**** current user => " + cu.username + "," + cu.name + "," + cu.email);
	//-------

	//-- 전체 게시물 수 구한 후 요청된 페이지 표시
	let token = req.cookies[__ACCESS_TOKEN_NAME];
	let _headers = {};
	util.log("request token => " + token);
	_headers[__ACCESS_TOKEN_NAME] = token;
	axios.get(__API_PRODUCT_URI + "/count", {
		headers: _headers
	})
	.then((ret) => {
		util.log("Success to get product count!");
		let data = ret.data;
		let count = (data.success == false ? 0 : data.value);
		renderPage(count);
	})
	.catch((error) => {
		console.error("Fail to get product count", error);
	});

	const renderPage = function (count) {
		const pageSize = 10;		//한 페이지당 문서수
		const pageListSize = 5;		//navaigator bar에 보일 페이지 수
	
		let totalCount = count;			//전체문서수
		if(totalCount < 0) totalCount = 0;

		//-- 전체 문서수에 따른 페이징 control 변수 셋팅
		let curPage = parseInt(req.params.cur);					//현재 페이지
	
		let totalPage = Math.ceil(totalCount / pageSize);		//전체 페이지수
		let totalSet = Math.ceil(totalPage / pageListSize); 	//전체 페이지 세트수
		let curSet = Math.ceil(curPage / pageListSize); 		//현재 셋트 번호
		let startPage = ((curSet - 1) * pageListSize) + 1; 		//현재 세트내 출력될 시작 페이지
		let endPage = (startPage + pageListSize) - 1; 			//현재 세트내 출력될 마지막 페이지
		let startNo = (curPage < 0 ? 0 : (curPage - 1) * pageSize);	//시작 일련 번호 

		let pagingControls = {
			"curPage": curPage,
			"pageListSize": pageListSize,
			"pageSize": pageSize,
			"totalPage": totalPage,
			"totalSet": totalSet,
			"curSet": curSet,
			"startPage": startPage,
			"endPage": endPage,
			"startNo": startNo
		};
		util.log(pagingControls);
		//---------

		//--- 요청된 페이지 데이터 표시
		axios.get(__API_PRODUCT_URI + "/entries", {
			headers: _headers,
			params: {
				order: ["id", "DESC"],
				offset: startNo,
				limit: pageSize
			}
		})
		.then((ret) => {
			let data = ret.data;
			if (data.success) {
				util.log("Success to get entries !");
				res.render("views/main", {
					data: data.value,
					paging: pagingControls,
					user: util.userData
				});
			} else {
				console.error("Fail to get entries", data.msg);
			}
		})
		.catch((error) => {
			console.error("Fail to get entries", error);
		});
	}
});
*/

module.exports = router;
