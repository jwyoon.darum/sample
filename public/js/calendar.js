$(document).ready(function(){
'use strict';
	// used in demos later on to merge default options with demo options...
	function extend(a, b) { // extend a with properties of b not in a; simple version
		for (var key in b) {
			if (!a.hasOwnProperty(key)) {
				a[key] = b[key];
			}
		}
		return a;
	}
	var options = {
		weekDays: ['일', '월', '화', '수', '목', '금', '토'],
		months: ['01', '02', '03', '04', '05', '06','07', '08', '09', '10', '11', '12'],
		datePickerClass: "date-picker popup-wrap",
		sundayBased: true,
		renderWeekNo: false,
		equalHight: true,
		weekDayClass: 'week-day', // not standard: used in template.start
		weekDayRowHead: '',
		header: '<div class="popup-top"><h2 class="tit">날짜선택</h2><button type="button" class="btn-close">팝업닫기</button></div><div class="popup-con"><div class="dp-title"><button class="dp-prev" type="button"{{disable-prev}}>{{prev}}</button><div class="dp-label-wrap"><div class="dp-label dp-label-year"><span>{{year}}</span><select class="dp-select dp-select-year" tabindex="-1">{{years}}</select></div><div class="dp-label dp-label-month"><span>{{month}}</span><select class="dp-select dp-select-month" tabindex="-1">{{months}}</select></div></div><button class="dp-next" type="button"{{disable-next}}>{{next}}</button></div>',
		template: {
			row: '<td class=""><span class=""{{event}} data-day=\'{"day":"{{day}}", "month":"{{month}}", "year":"{{year}}"}\'>{{day-event}}{{today}}</span></td>',
			start: function (month, year) { // rendering week days in table header
				var options = this.options,
					weekDayRow = '<th class="">{{day}}</th>',
					row = [];

				if (options.renderWeekNo) { // week number head
					row.push(weekDayRow.replace(/{{day}}/g, options.weekDayRowHead));
				}

				for (var n = 0, dayOfWeek = 0; n < 7; n++) { // week days
					dayOfWeek = n + (options.sundayBased ? 0 : (n === 6 ? -6 : 1));
					row.push(weekDayRow.replace(/class="(.*?)"/, function ($1, $2) {
						return 'class="' + options.weekDayClass + ' ' +
							(options.workingDays.indexOf(dayOfWeek) === -1 ?
								options.weekEndClass : '') + '"';
					}).replace(/{{day}}/g, options.weekDays[dayOfWeek]));
				}
				return '<table class="cal-month"><thead><tr>' +
					row.join('') +
					'</tr></thead><tbody><tr>';
			},

			event: function (day, date, event) { // rendering events
				var text = [],
					uuids = [],
					someExtra = '';

				for (var n = 0, m = event.length; n < m; n++) {
					event[n].text && text.push('- ' + event[n].text);

					uuids.push(event[n]._id);

					if (event[n].extra) { // extend functionality...
						someExtra = event[n].extra;
					}
				}
				text = text.join("\n");

				return text ? ' title="' + text + '"' +
					' data-uuids=\'[' + uuids.join(', ') + ']\'' +
					(someExtra ? ' data-name="' + someExtra + '"' : '') : '';
			},
			day: function (day, date, event) { // rendering every day
				var length = event.length;

				for (var n = length; n--;) { // check if it's only a 'disabled' event
					if (event[n].type && event[n].type === 'disabled') { // or event[n].disabled
						length--;
					}
				}
				if (length > 1) {
					return day + '<span class="count-icon">' + length + '</span>';
				}
			}
		}
	};

	$('#calendarStart').datePicker(options); //검색 시작일
	$('#calendarEnd').datePicker(options); //검색 종료일
});

