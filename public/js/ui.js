// Common _ footer bar / top btn
$.fn.scrollEnd = function (callback, timeout) {
    $(this).scroll(function () {
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback, timeout));
    });
};

$(window).scrollEnd(function () {
    if ($('.btn-pagetop').length > 0) {
        if ($(this).scrollTop() > 50) {
            $('.btn-pagetop').addClass('active');
        } else {
            $('.btn-pagetop').removeClass('active');
        }

        $('.btn-pagetop').click(function () {
            var pTop = $($(this).attr('href')).offset().top;
            $('html, body').animate({ scrollTop: pTop }, 200);
            return false;
        });

        direction = "";
    }
}, 300);

var didScroll;
var direction = "";
$(window).scroll(function () {

    $('.btn-pagetop').removeClass('active');

    var i = navigator.userAgent;

    if (/Chrome/i.test(i) || /CriOS/i.test(i)) {

    } else if (/Safari/i.test(i)) {
        if ((document.documentElement.clientHeight + 114) >= document.body.scrollHeight - $(this).scrollTop()) {
            return;
        }
    }

    //2019-05-30 독바 스크립트
    if ($(this).scrollTop() < didScroll || $(this).scrollTop() <= 0) {
        if (direction != "UP") {
            $('.footer-bar').css({
                bottom: '0px',
                transition: 'bottom ease-in-out 0.5s'
            });
        }
        direction = "UP";
    } else {
        if (direction != "DOWN") {
            $('.footer-bar').css({
                bottom: '-70px',
                transition: 'bottom ease-in-out 0.5s'
            });
        }
        direction = "DOWN";
    }
    didScroll = $(this).scrollTop();

});

$(document).ready(function(){
    

    // ===================================================================================== //

    


    /*
    * Common UI
    */

    //datepicker
    $('input[name="daterange"]').on('click',function(){
        location.href='./00_popup_calendar.html';
    });

    $('.search-box .btn.arrow').on('click',function(){
        $(this).toggleClass('active');
        $('.search-box .detail').toggleClass('hidden');
    });
    // dim 클릭시 
    $('.dim').on('click',function(){
        console.log(' DIM CLICK ');
        $('.popup-wrap').removeClass('active');
    });
    // 박스 넓이 설정
    $('.ctr-radio .box').each(function(){
        var boxW = parseFloat(100 / $(this).parent().find('li').length)+'%';
        $(this).css('width', boxW);
    })
    // 박스 움직임 설정
    $('.ctr-radio li input').on('click',function() {
        moveBox($(this));
    });

    var moveBox = function(obj){
        var moveLeft = obj.offset().left - 21;
        obj.parent().parent().next('.box').css('left', moveLeft);
    }

    // 뒤로가기 버튼
    $('.btn-back').on('click',function(){
        history.back();
    });
    // 달력 팝업 닫기 버튼
    $('.date-picker .btn-close').on('click',function(){
        $('.date-picker').hide();
    });
    
});

// LayerPopup
function openPopup(id){
    $('#' + id).addClass('active');
    $('footer').before('<div class="dim"></div>');
    return false;
}
function closePopup(id){
    $('#'+id).removeClass('active');
    $('.dim').remove();
    return false;
}

// 조회 조건
function searchComplete(){
    var srchTxtArray = new Array;
    // 체크된 값 배열입력
    $('#searchCondition input').each(function(){
        if( $(this).prop('checked') == true) srchTxtArray.push($(this).next('label').text());
    });
    closePopup('searchCondition');
    // 결과값 전달(달력 제외)
    for (i = 0; i < $('.sort span').length ; i++) {
        $('#sortCate' + (i + 1)).text(srchTxtArray[i]);
    }
    var calStartDay = $('#calendarStart').prop('value');
    var calEndDay = $('#calendarEnd').prop('value');
    $('.sort-wrap .search-date').text(calStartDay + '~' + calEndDay);
}


var viewKorean = function(num) { 
    var hanA = new Array("","일","이","삼","사","오","육","칠","팔","구","십"); 
    var danA = new Array("","십","백","천","","십","백","천","","십","백","천","","십","백","천"); 
    var result = ""; 
    num = num.replace(/\,/g, '');
    
    for(i=0; i<num.length; i++) 
    { 
        str = ""; 
        han = hanA[num.charAt(num.length-(i+1))]; 

        if(han != "") 
        {
            str += (danA[i]=="천" ? " " : "")+han+danA[i]; 
        }
        if(i == 4) str += "만"; 
        if(i == 8) str += "억"; 
        if(i == 12) str += "조"; 
        result = str + result; 
    } 

    if(num != 0) result = result + "원"; 
    return result ; 
}


var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(".money_info").on("keyup",function(){
    var this_val = $(this).val();
    $(this).val(numberWithCommas(this_val));
})